<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\ProductsPrices;
use Khill\Lavacharts\Lavacharts;

class Charts extends Controller {

    public function index(Request $request) {
        $method = $request->input('m');
        $product_id = $request->input('p');
        
        $prices = ProductsPrices::getDataForChart($product_id);
        
        if ($method == 1) {
            $chart = \Lava::DataTable();
            $chart->addDateColumn('Date')
                    ->addNumberColumn('First method');

            foreach ($prices['first_method'] as $price) {
                $tmp[$price->start_date] = $price->price;
                $tmp[$price->end_date] = $price->price;
            }
            ksort($tmp);


            foreach ($tmp as $date => $price) {
                $chart->addRow([$date, $price]);
            }
            \Lava::LineChart('chart', $chart, [
                'title' => 'Price chart'
            ]);
            
            echo '<div id="temps_div"></div>';
            echo \Lava::render('LineChart', 'chart', 'temps_div');
        } else {
            $chart = \Lava::DataTable();
            $chart->addDateColumn('Date')
                    ->addNumberColumn('Second method');
            
            foreach ($prices['first_method'] as $price) {
                $tmp2[$price->price_date_id][$price->start_date] = $price->price;
                $tmp2[$price->price_date_id][$price->end_date] = $price->price;
            }
            krsort($tmp2);

            foreach ($tmp2 as $date) {
                foreach ($date as $date => $price) {
                    $chart->addRow([$date, $price]);
                }
            }
            \Lava::LineChart('chart', $chart, [
                'title' => 'Price chart'
            ]);
            
            echo '<div id="temps_div2"></div>';
            echo \Lava::render('LineChart', 'chart', 'temps_div2');
        }
    }

}
