<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\ProductsPrices;

class PriceInterface extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prices = ProductsPrices::all();
        $currentPrices = ProductsPrices::getCurrentPrices();

        return view('welcome', compact('prices', 'currentPrices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(count($request->new_start_date) == count($request->new_end_date)){
            for($i = 0; $i < count($request->new_start_date); $i++){
                $newPrice = new ProductsPrices;
                $newPrice->product_id = $request->product_id[$i];
                $newPrice->start_date = $request->new_start_date[$i];
                $newPrice->end_date = $request->new_end_date[$i];
                $newPrice->price = $request->new_price[$i];
                $newPrice->save();
            }     
        }
        
        if (count($request->start_date) == count($request->end_date)){
            for($i = 0; $i < count($request->start_date); $i++){
                $product = ProductsPrices::where('price_date_id', $request->price_id[$i])->update(['start_date' => $request->start_date[$i], 'end_date' => $request->end_date[$i], 'price' => $request->price[$i]]);
            }
        }
     return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        ProductsPrices::where('price_date_id', $request->data_id)->delete();
        return $request->data_id;
    }
}
