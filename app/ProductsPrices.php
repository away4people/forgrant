<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductsPrices extends Model
{
    protected $table = 'products_prices';
    public $primarykey = 'price_date_id';
    
    public static function getCurrentPrices(){
        $currentPrices[] = DB::select('SELECT pr.product_id, pr.title, pr.description, 
                (SELECT pr_da.price FROM products_prices as pr_da WHERE pr_da.product_id=pr.product_id AND (CURDATE() BETWEEN pr_da.start_date AND pr_da.end_date) 
                ORDER BY (UNIX_TIMESTAMP(pr_da.end_date)-UNIX_TIMESTAMP(pr_da.start_date)) ASC LIMIT 1) AS price
                FROM products AS pr');
        
        $currentPrices[] = DB::select('SELECT pr.product_id,
                (SELECT pr_da.price FROM products_prices as pr_da WHERE pr_da.product_id=pr.product_id AND (CURDATE() BETWEEN pr_da.start_date AND pr_da.end_date) ORDER BY pr_da.price_date_id DESC LIMIT 1) AS price
                FROM products AS pr');
//        dd($currentPrices);
        return $currentPrices;
    }
    
    public static function getDataForChart($product_id){
        $data['first_method'] = DB::select('SELECT * FROM `products_prices` WHERE product_id = ? ', [$product_id]);
        $data['second_method'] = DB::select('SELECT * FROM `products_prices` WHERE product_id = ? ORDER BY price_date_id DESC', [$product_id]);
        return $data;
    }
}
