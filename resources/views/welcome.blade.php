<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                @foreach($currentPrices[0] as $currentPrice)
                
                    ID: {{ $currentPrice->product_id }}
                    <br> Title: {{ $currentPrice->title }}
                    <br> Description: {{ $currentPrice->description }}
                    <br> Diapasones:
                    <br>
                    <form method="POST" action="/priceChange">
                        <div id='diapasone-{{$currentPrice->product_id}}'>
                        @for($i = 0; $i < count($prices); $i++)
                                @if ($currentPrice->product_id == $prices[$i]['product_id']) 
                                    Start date: <input type='date' name='start_date[]' value='{{$prices[$i]['start_date']}}'>  
                                    <a href='' data-id='{{$prices[$i]['price_date_id']}}' class='priceRemove' style='text-decoration: none;color: black;    font-size: large;'>X</a>
                                    <br>
                                    End date:   <input type='date' name='end_date[]' value='{{$prices[$i]['end_date']}}'>    <br>
                                    Price :     <input type="number" min="0" step="1" name='price[]' value='{{$prices[$i]['price']}}'>    <br>
                                    Created at: {{$prices[$i]['created_at']}}   <br>   
                                    Updated at: {{$prices[$i]['updated_at']}}    <br>
                                    
                                    <input type="hidden" name='price_id[]' value="{{$prices[$i]['price_date_id']}}">
                                @endif
                                <br />
                            @endfor
                        </div>
                            <a href ='' class='addDiapasone' data-id='{{$currentPrice->product_id}}' class='addDiapasone' style='text-decoration: none;color: black;    font-size: large;'>Add diapasone</a> <br>
                        <input type="hidden" name='product_id' value="{{$currentPrice->product_id}}">
                        {{ csrf_field() }}
                        <button type="submit">Change</button> 
                    </form>
                    Current price(<a href='/charts?p={{$currentPrice->product_id}}&m=1'> method-1 </a>): {{$currentPrice->price}} <br>
                    @foreach($currentPrices[1] as $currentPrice2)
                        @if ($currentPrice2->product_id == $currentPrice->product_id)
                        Current price(<a href='/charts?p={{$currentPrice->product_id}}&m=2'> method-2 </a>): {{$currentPrice2->price}}
                        @endif
                    @endforeach
                    <hr>
                    
                @endforeach
            </div>
        </div>
        
        
        <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                $('.addDiapasone').click(function(){
                    $('#diapasone-'+$(this).attr("data-id")).append('Start date: <input type="date" name="new_start_date[]"> <br>'+
                                                                    'End date:   <input type="date" name="new_end_date[]">    <br>' + 
                                                                    'Price :     <input type="number" min="0" step="1" name="new_price[]"> <br>');
                   return false; 
                });
                
                
               $('.priceRemove').click(function(){
                  $.ajax({
                    type:'POST',
                    url:'/priceRemove',
                    data:{'data_id' : $(this).attr('data-id'), '_token' : '<?php echo csrf_token() ?>' },
                    success:function(data){
                       console.log(data);
                       window.location.replace("/");
                    },
                    error:function(data){
                       console.log(data);
                    }
                 });
                 return false;
               });
            });
        </script>
    </body>
</html>
