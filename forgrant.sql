-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 15 2018 г., 19:03
-- Версия сервера: 5.6.37
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `forgrant`
--

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`product_id`, `title`, `description`) VALUES
(1, 'Школьная форма', 'Описание'),
(2, 'Школьная форма-2', 'Описание-2');

-- --------------------------------------------------------

--
-- Структура таблицы `products_prices`
--

CREATE TABLE `products_prices` (
  `price_date_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `price` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products_prices`
--

INSERT INTO `products_prices` (`price_date_id`, `product_id`, `start_date`, `end_date`, `created_at`, `updated_at`, `price`) VALUES
(3, 2, '2018-09-01', '2018-10-12', '2018-10-13', '2018-10-15', '750'),
(4, 2, '2018-06-01', '2018-12-01', '2018-10-13', '2018-10-15', '1500'),
(9, 1, '2018-10-11', '2018-10-26', '2018-10-15', '2018-10-15', '750'),
(10, 1, '2018-10-01', '2018-12-07', '2018-10-15', '2018-10-15', '800'),
(11, 2, '2018-01-01', '2018-12-12', '2018-10-15', '2018-10-15', '500');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Индексы таблицы `products_prices`
--
ALTER TABLE `products_prices`
  ADD PRIMARY KEY (`price_date_id`),
  ADD KEY `start_date` (`start_date`,`end_date`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `products_prices`
--
ALTER TABLE `products_prices`
  MODIFY `price_date_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
